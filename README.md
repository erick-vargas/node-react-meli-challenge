## Erick Vargas.
*Test Práctico - Frontend / Mercado Libre*

---

## Resumen

La aplicación consta de tres componentes principales: la caja de búsqueda, la visualización de resultados, y la
descripción del detalle del producto. Consulta tres endpoint generados en node/express los cuales consultan servicios publicos de Mercado Libre. En el front se puede hacer seguimiento de todos los eventos a través del inspector de REDUX. 

---

Stack tecnológico de la aplicación:
> Cliente:
1. HTML
2. React
3. React Router
4. Redux
5. SASS
6. Jest/Enzyme
> Servidor:
1. Node >= 6
2. Express

---

## Instalacion

> Cliente:
1. Desde la raiz del proyecto ejecutar en la consola
2. npm install
3. npm audit fix. (si existiera alguna dependencia desactualizada)
4. npm start
5. La aplicacion abrirá en el navegador en el puerto 3000
> Servidor:
1. Desde la ruta ./src/server del proyecto ejecutar en la consola
2. npm install
3. npm audit fix. (si existiera alguna dependencia desactualizada)
4. npm start
5. La aplicacion abrirá en el navegador en el puerto 4000

---

## Uso

Existen tres pantallas navegables, el buscador, el listado de resultados y el detalle deproducto.

## Enpoints disponibles

1. /* GET products listing. */- get("/items")
2. /* GET product details. */ - get("/items/:id")
3. /* GET categories. */------- get("/categories/:id")

---

## Aditional

Para el namming de las clases utilicé el estandar BEM, 

1. http://getbem.com/naming/

Agregue un endpoint para las categorias para un mejor manejo de la construccion del breadcrum, basandome en la documentacion de la api de mercadolibre

1. https://developers.mercadolibre.com.ar/es_ar/categoriza-productos

---

---

## Contacto
    tel: +5491126993566
    erickorso@gmail.com
    
    https://bitbucket.org/erick-vargas/node-react-meli-challenge/src/master/

---
---