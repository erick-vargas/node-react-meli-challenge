import React from "react";
import "../CatalogView/style.scss";
import Search from "../../components/Search";
import Notification from "../../components/shared/Notification";

export default () => (
  <main className="meli-page catalog-page">
    <Search />
    <div className="catalog-page__wrapper">
        <div style={{background: '#fff', minHeight: '20vh', marginTop: '32px', borderRadius: '4px'}}>
          <Notification title="Error 404: Página no encontrada. Intenta escribir algo en la barra de busqueda."/>
        </div>
    </div>
  </main>
);
