import React from "react";
import { shallow } from "enzyme";
import NoMatchView from ".";
import Search from "../../components/Search";
import Notification from "../../components/shared/Notification";

describe("NoMatchView component", () => {
  it("should match the snapshot", () => {
    const tree = shallow(<NoMatchView/>);
    expect(tree).toMatchSnapshot();
  });
  
  it("should render Search", () => {
    const wrapper = shallow(<NoMatchView/>);
    const find = wrapper.find(Search);
    expect(find).toHaveLength(1);
  });

  it("should render Notification", () => {
    const wrapper = shallow(<NoMatchView/>);
    const find = wrapper.find(Notification);
    expect(find).toHaveLength(1);
  });
  
});
