import React from "react";
import { shallow } from "enzyme";
import CatalogView from ".";
import Search from "../../components/Search";
import Breadcrum from "../../components/Breadcrum";
import Catalog from "../../components/Catalog";

describe("CatalogView component", () => {
  it("should match the snapshot", () => {
    const tree = shallow(<CatalogView store={{}}/>);
    expect(tree).toMatchSnapshot();
  });
  
  it("should render Search", () => {
    const wrapper = shallow(<CatalogView store={{}}/>);
    const find = wrapper.find(Search);
    expect(find).toHaveLength(1);
  });

  it("should render Breadcrum", () => {
    const wrapper = shallow(<CatalogView store={{}}/>);
    const find = wrapper.find(Breadcrum);
    expect(find).toHaveLength(1);
  });

  it("should render Catalog", () => {
    const wrapper = shallow(<CatalogView store={{}}/>);
    const find = wrapper.find(Catalog);
    expect(find).toHaveLength(1);
  });
  
});
