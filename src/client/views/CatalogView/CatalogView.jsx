import React from "react";
import "./style.scss";
import Search from "../../components/Search";
import Breadcrum from "../../components/Breadcrum";
import Catalog from "../../components/Catalog";

export default () => (
  <main className="meli-page catalog-page">
    <Search />
    <div className="catalog-page__wrapper">
      <Breadcrum />
      <Catalog />
    </div>
  </main>
);
