import ProductView from "./ProductView";
import { withRouter } from "react-router";
import { connect } from "react-redux";

import { getCatById, getProductById } from "../../redux/services/productService";
import {
    getErrorDetailState,
    getLoadingDetailState,
    getProductDetailState,
} from "../../redux/selectors/productSelector";

const mapStateToProps = (state) => ({
  productStore: getProductDetailState(state),
  loadingStore: getLoadingDetailState(state),
  errorStore: getErrorDetailState(state),
});

const mapDispatchToProps = (dispatch) => ({
  getProduct: (id) => getProductById(id, dispatch),
  getCat: (id) => getCatById(id, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProductView));
