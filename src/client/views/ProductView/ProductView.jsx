import React, { useState, useEffect } from "react";
import Search from "../../components/Search";
import Breadcrum from "../../components/Breadcrum";
import ProductDetail from "../../components/ProductDetail";
import "./style.scss";

export default ({ productStore, loadingStore, errorStore, getProduct, match }) => {
  const { params: { id } } = match;

  const [matchId, setMatchId] = useState(null);
  const [product, setProduct] = useState(null);

  useEffect(() => {
    if (id) {
      setMatchId(id);
    }
  }, [ id, setMatchId ]);

  useEffect(() => {
    if (matchId && !productStore && !product) {
      getProduct(matchId);
    }
  }, [matchId, productStore, getProduct, product]);

  useEffect(() => {
    if (productStore && !product) {
      setProduct(productStore);
    }
  }, [ productStore, setProduct, product ]);
  
  return (
    <div className="meli-page product-detail-page">
      <Search />
      <div className="product-detail-page__wrapper">
        <Breadcrum/>
        <ProductDetail productDetails={{productStore, loadingStore, errorStore}}/>
      </div>
    </div>
  );
};
