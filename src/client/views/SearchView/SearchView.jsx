import React from "react";
import "./style.scss";
import Search from "../../components/Search";

export default () => (
  <main className="meli-page search-page">
    <Search />
  </main>
);
