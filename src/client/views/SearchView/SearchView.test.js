import React from "react";
import { shallow } from "enzyme";
import SearchView from ".";
import Search from "../../components/Search";

describe("SearchView component", () => {
  it("should match the snapshot", () => {
    const tree = shallow(<SearchView/>);
    expect(tree).toMatchSnapshot();
  });
  
  it("should render Search", () => {
    const wrapper = shallow(<SearchView/>);
    const find = wrapper.find(Search);
    expect(find).toHaveLength(1);
  });
  
});
