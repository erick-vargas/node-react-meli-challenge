import getOr from "lodash/fp/getOr";

export const getProductsState = getOr([], "products.data")
export const getAuthorState = getOr(null, "products.author")
export const getLoadingState = getOr(null, "products.loading")
export const getErrorState = getOr(null, "products.error")

export const getProductDetailState = getOr(null, "product.data.item");
export const getLoadingDetailState = getOr(null, "product.loading");
export const getErrorDetailState = getOr(null, "product.error");

export const getCatState = getOr(null, "category.data.path_from_root");
export const getLoadingCatState = getOr(null, "category.loading");
export const getErrorCatState = getOr(null, "category.error");

