import K from "../constants";
const { GET_PRODUCTS, GET_PRODUCT, GET_CATS } = K;

export const getProductAction = (product, dispatch) => {
  dispatch({
    type: GET_PRODUCT,
    product,
  });
};

export const getProductsAction = (products, dispatch) => {
  dispatch({
    type: GET_PRODUCTS,
    products,
  });
};

export const getCatAction = (cat, dispatch) => {
  dispatch({
    type: GET_CATS,
    cat,
  });
};
