export const serviceStartAction = (type, dispatch) =>
  dispatch({ type: `${type}_START` });

export const serviceErrorAction = (type, error, dispatch) =>
  dispatch({ type: `${type}_ERROR`, error });

export const serviceSuccessAction = (type, dispatch) =>
  dispatch({ type: `${type}`, error: null, loading: null });
