import K from "../constants";
const { GET_PRODUCT } = K;

const stateDefault = {
  data: null,
  id: '',
  error: null,
  loading: null,
};

export default (state = stateDefault, action) => {
  switch (action.type) {
    case GET_PRODUCT:
      return {
        ...stateDefault,
        data: action.product,
      };

    case `${GET_PRODUCT}_START`:
      return {
        ...stateDefault,
        id: action.q,
        loading: true,
      };

    case `${GET_PRODUCT}_ERROR`:
      return {
        ...stateDefault,
        error: true,
      };

    default:
      break;
  }

  return state;
};
