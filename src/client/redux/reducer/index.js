import { combineReducers } from "redux";
import products from "./products";
import product from "./product";
import category from "./category";

export default combineReducers({
    products,
    product, 
    category,
});
