import K from "../constants";
const { GET_CATS } = K;

const stateDefault = {
  data: null,
  error: null,
  loading: null,
};

export default (state = stateDefault, action) => {
  switch (action.type) {
    case GET_CATS:
      return {
        ...stateDefault,
        data: action.cat
      };

    case `${GET_CATS}_START`:
      return {
        ...stateDefault,
        loading: true,
      };

    case `${GET_CATS}_ERROR`:
      return {
        ...stateDefault,
        error: true,
      };

    default:
      break;
  }

  return state;
};
