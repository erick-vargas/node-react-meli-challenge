import K from "../constants";
const { GET_PRODUCTS } = K;

const stateDefault = {
  data: [],
  author: null,
  categories: [],
  error: null,
  loading: null,
};

export default (state = stateDefault, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return {
        ...stateDefault, 
        data: action.products.items,
        author: action.products.author,
        categories: action.products.categories,
      };

    case `${GET_PRODUCTS}_START`:
      return {
        ...stateDefault,
        loading: true,
      };

    case `${GET_PRODUCTS}_ERROR`:
      return {
        ...stateDefault,
        error: true,
      };

    default:
      break;
  }

  return state;
};
