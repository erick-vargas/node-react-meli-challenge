import {
  serviceStartAction,
  serviceErrorAction
} from "../actions/commonAction";
import { getCatAction, getProductAction, getProductsAction } from "../actions/productAction";

import K from "../constants";
const { GET_PRODUCTS, GET_PRODUCT, GET_CATS } = K;

const urls = {
  SEARCH_PRODUCTS_URL: "http://localhost:4000/api/items?search=",
  GET_PRODUCT_BY_ID_URL: "http://localhost:4000/api/items/",
  GET_CATS_BY_ID_URL: "http://localhost:4000/api/categories/",
};

export const searchProductService = (value, dispatch) => {
  serviceStartAction(GET_PRODUCTS, dispatch);
  fetch(urls.SEARCH_PRODUCTS_URL + value)
    .then((response) => response.json())
    .then((products) => {
      getProductsAction(products, dispatch);
      let mapCat = products.categories.map(item => ({name: item}))
      getCatAction({ path_from_root: mapCat }, dispatch);
      return;
    })
    .catch((error) => serviceErrorAction(GET_PRODUCTS, error, dispatch));
};

export const getProductById = (id, dispatch) => {
  serviceStartAction(GET_PRODUCT, dispatch);
  fetch(urls.GET_PRODUCT_BY_ID_URL + id)
    .then((response) => response.json())
    .then((product) => {
      getProductAction(product, dispatch);
      if (product.item.category_id) {
        getCatById(product.item.category_id, dispatch);
      }
      return;
    })
    .catch((error) => serviceErrorAction(GET_PRODUCT, error, dispatch));
};

export const getCatById = (id, dispatch) => {
  serviceStartAction(GET_CATS, dispatch);
  fetch(urls.GET_CATS_BY_ID_URL + id)
    .then((response) => response.json())
    .then((cat) => {
      getCatAction(cat.data, dispatch);
      return;
    })
};
