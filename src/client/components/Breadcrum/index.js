import Breadcrum from "./Breadcrum";
import { withRouter } from "react-router";
import { connect } from "react-redux";

import {
  getCatState,
  getErrorCatState,
  getLoadingCatState,
} from "../../redux/selectors/productSelector";
import { searchProductService } from "../../redux/services/productService";

const mapStateToProps = (state) => ({
  categories: getCatState(state),
  loading: getLoadingCatState(state),
  error: getErrorCatState(state)
});

const mapDispatchToProps = (dispatch) => ({
  searchProduct: (value) => searchProductService(value, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Breadcrum));
