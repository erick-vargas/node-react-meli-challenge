import React from "react";
import "./style.scss";

export default ({ categories, loading, error, searchProduct, history }) => {
  const handleClick = (value) => {
    searchProduct(value);
    history.push("/items?search=" + value);
  };
  return (
    <nav className="breadcrum">
      {error ? (
        <span className="error">..</span>
      ) : (
        <>
          <ul className="breadcrum__list">
            {loading && <li className="breadcrum__item loading">...</li>}
            {!loading &&
              categories &&
              categories.map((item, key) => (
                <li className="breadcrum__item" key={key}>
                  <span onClick={() => handleClick(item.name)}>
                    {item.name}
                  </span>
                </li>
              ))}
          </ul>
        </>
      )}
    </nav>
  );
};
