import React from "react";
import { shallow } from "enzyme";
import Breadcrumb from "./Breadcrum";


describe("breadcrumb component", () => {
  const defaultProps = {
    categories: [
      { name: "categoriy1" },
      { name: "categoriy2" },
      { name: "categoriy3" },
    ],
    loading: false,
    error: false,
  };

  it("should match the snapshot", () => {
    const tree = shallow(<Breadcrumb categories={defaultProps.categories} />);
    expect(tree).toMatchSnapshot();
  });

  it("should match the snapshot onError", () => {
    const tree = shallow(<Breadcrumb error />);
    expect(tree).toMatchSnapshot();
  });

  it("should match the snapshot onLoading", () => {
    const tree = shallow(<Breadcrumb loading/>);
    expect(tree).toMatchSnapshot();
  });

  it("should find .. onError", () => {
    const wrapper = shallow(<Breadcrumb error />);
    const find = wrapper.find("span").text();
    expect(find).toBe('..')
  });

  it("should find .. onLoading", () => {
    const wrapper = shallow(<Breadcrumb loading />);
    const find = wrapper.find(".loading").text();
    expect(find).toBe("...");
  });

  it("should find ... onLoading", () => {
    const wrapper = shallow(
      <Breadcrumb categories={defaultProps.categories} />
    );
    const find = wrapper.find(".breadcrum__list").text();
    expect(find).toBe("categoriy1categoriy2categoriy3");
  });

});
