import React from 'react';
import './style.scss';
import lupa from "../../../assets/img/lupa.jpg";

export default ({title, icon}) => {
    if(!icon) icon = lupa;
    return (
      <div className="notification">
        <img src={icon} alt="logo" />
        <h2>{title}</h2>
      </div>
    );
    
}