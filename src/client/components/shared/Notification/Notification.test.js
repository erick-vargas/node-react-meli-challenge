import React from "react";
import { shallow, mount } from "enzyme";
import Notification from ".";

describe("Notification component", () => {

  const title = 'mi title';

  it("should match the snapshot", () => {
    const tree = shallow(<Notification title={title} />);
    expect(tree).toMatchSnapshot();
  });

  it("should render 2 Img", () => {
    const wrapper = mount(<Notification title={title} />);
    const find = wrapper.find('img');
    expect(find).toHaveLength(1);
  });

  it("should render a title", () => {
    const wrapper = mount(<Notification title={title} />);
    const find = wrapper.find("h2").text();
    expect(find).toBe("mi title");
  });
  
});
