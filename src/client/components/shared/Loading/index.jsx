import React from "react";
import logo from "../../../assets/img/logo.svg";

export default () => (
  <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
    </header>
  </div>
);
