import React from "react";
import { shallow, mount } from "enzyme";
import Loading from ".";

describe("Loading component", () => {

  it("should match the snapshot", () => {
    const tree = shallow(<Loading />);
    expect(tree).toMatchSnapshot();
  });

  it("should render 2 Img", () => {
    const wrapper = mount(<Loading />);
    const find = wrapper.find('img');
    expect(find).toHaveLength(1);
  });
  
});
