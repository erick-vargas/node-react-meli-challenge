import React from "react";
import { shallow, mount } from "enzyme";
import Tooltip from ".";

describe("Tooltip component", () => {

  const title = 'mi title';
  const children= 'some child';

  it("should match the snapshot", () => {
    const tree = shallow(<Tooltip title={title} children={children} />);
    expect(tree).toMatchSnapshot();
  });
  
  it("should render a title", () => {
    const wrapper = mount(<Tooltip title={title} children={children} />);
    const find = wrapper.find(".tooltip__title").text();
    expect(find).toBe("mi title");
  });
  
  it("should render text child", () => {
    const wrapper = mount(<Tooltip title={title} children={children} />);
    const find = wrapper.find('.tooltip').text();
    expect(find).toBe("mi titlesome child");
  });
  
});
