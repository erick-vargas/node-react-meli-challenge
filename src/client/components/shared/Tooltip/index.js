import React from 'react';
import './style.scss';

export default ({ title, children }) => (
  <div className="tooltip">
    <div className="tooltip__title">{title}</div>
    {children}
  </div>
);