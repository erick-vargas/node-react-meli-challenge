import Catalog from "./Catalog";
import { withRouter } from "react-router";
import { connect } from "react-redux";

import { getProductById } from "../../redux/services/productService";
import { getErrorState, getLoadingState, getProductsState } from "../../redux/selectors/productSelector";

const mapStateToProps = (state) => ({
  productsStore: getProductsState(state),
  loadingStore: getLoadingState(state),
  errorStore: getErrorState(state),
});

const mapDispatchToProps = (dispatch) => ({
  getProduct: (value) => getProductById(value, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Catalog));
