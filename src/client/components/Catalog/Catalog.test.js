import React from "react";
import { shallow } from "enzyme";
import Catalog from "./Catalog";
import ProductCard from "../ProductCard";
import Notification from "../shared/Notification";

describe("Catalog component", () => {
  const defaultProps = {
    items: [{ name: "item1" }, { name: "item2" }, { name: "item3" }],
  };

  it("should match the snapshot", () => {
    const tree = shallow(<Catalog productsStore={defaultProps.items} />);
    expect(tree).toMatchSnapshot();
  });

  it("should match the snapshot onError", () => {
    const tree = shallow(
      <Catalog errorStore productsStore={defaultProps.items} />
    );
    expect(tree).toMatchSnapshot();
  });

  it("should match the snapshot onLoading", () => {
    const tree = shallow(
      <Catalog loadingStore productsStore={defaultProps.items} />
    );
    expect(tree).toMatchSnapshot();
  });

  it("should find 1 onError", () => {
    const wrapper = shallow(
      <Catalog errorStore productsStore={defaultProps.items} />
    );
    const find = wrapper.find(Notification);
    expect(find).toHaveLength(1);
  });

  it("should find 1 onLoading", () => {
    const wrapper = shallow(
      <Catalog loadingStore productsStore={defaultProps.items} />
    );
    const find = wrapper.find(Notification);
    expect(find).toHaveLength(1);
  });

  it("should find 3 onList", () => {
    const wrapper = shallow(<Catalog productsStore={defaultProps.items} />);
    const find = wrapper.find(ProductCard);
    expect(find).toHaveLength(3);
  });
});
