import React from "react";
import ProductCard from "../ProductCard";
import Notification from "../shared/Notification";
import "./style.scss";

export default ({ productsStore, loadingStore, errorStore, getProduct }) => (
  <section className="catalog">
    {loadingStore ? (
      <Notification title="Cargando." />
    ) : (
      <>
        {productsStore.length > 0 && (
          <ul className="catalog__list">
            {productsStore.map((item, key) => (
              <li className="catalog__item" key={key}>
                <ProductCard product={item} action={getProduct} />
              </li>
            ))}
          </ul>
        )}
      </>
    )}
    {productsStore.length === 0 && !loadingStore && (
      <Notification title="No hay publicaciones que coincidan con tu búsqueda." />
    )}
    {errorStore && !loadingStore && (
      <Notification title="Hubo un error, intenta otra busqueda." />
    )}
  </section>
);
