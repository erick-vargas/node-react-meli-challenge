import React from "react";
import { shallow, mount } from "enzyme";
import ProductCard from "./ProductCard";

describe("ProductCard component", () => {
  const product = {
    id: '123',
    thumbnail: '456',
    price: '789',
    title: 'my title',
    city: 'my city',
    free_shipping: 'true',
  };

  it("should match the snapshot", () => {
    const tree = shallow(<ProductCard product={product}/>);
    expect(tree).toMatchSnapshot();
  });

  it("should render 2 Img", () => {
    const wrapper = mount(<ProductCard product={product} />);
    const find = wrapper.find('img');
    expect(find).toHaveLength(2);
  });
});
