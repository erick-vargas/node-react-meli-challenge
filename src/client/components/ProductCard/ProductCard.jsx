import React, {useState, useEffect} from "react";
import iconShipping from "../../assets/img/ic_shipping.png";
import Tooltip from "../shared/Tooltip";

import "./style.scss";

export default ({ product, history, action }) => {
  const { id, thumbnail, price, title, city, free_shipping } = product;
  const [currentProduct, setCurrentProduct] = useState(null);

  const formatNumber = (number) =>
    new Intl.NumberFormat("de-DE").format(number);

  useEffect(() => {
    if (id) {
      setCurrentProduct(product);
    }
  }, [id, setCurrentProduct, product]);

  const handleClick = (id) => {
    action(id);
    history.push(`/items/${id}`);
  };

  const showCurrency = (currency) => (currency === "ARS" ? "$" : currency);

  return (
    <article className="product-card" onClick={() => handleClick(id)}>
      {currentProduct && (
        <>
          <div
            className="product-card__image"
            style={{ backgroundImage: `url(${thumbnail})` }}
          >
            <div className="product-card__image__hover">
              <img src={thumbnail} alt="logo" />
            </div>
          </div>
          <div className="product-card__details">
            <div className="product-card__details--left">
              <div className="price">
                {showCurrency(price.currency)}&nbsp;
                {formatNumber(parseInt(price.amount))}
                {free_shipping && (
                  <Tooltip title="Free Shipping">
                    <img src={iconShipping} alt="shipping" />
                  </Tooltip>
                )}
              </div>
              <h2 className="title">{title}</h2>
            </div>
            <div className="product-card__details--right">
              <div className="city">{city}</div>
            </div>
          </div>
        </>
      )}
    </article>
  );
};
