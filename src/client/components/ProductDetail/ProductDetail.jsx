import React, {useState, useEffect} from "react";
import iconShipping from "../../assets/img/ic_shipping.png";
import Tooltip from "../shared/Tooltip";

import "./style.scss";
import Notification from "../shared/Notification";

export default ({ productDetails, history }) => {
  const [product, setProduct] = useState(null);

  const { productStore, loadingStore, errorStore } = productDetails;

  const formatNumber = (number) =>
    new Intl.NumberFormat("de-DE").format(number);

  const isNew = (bool) => (bool ? "Nuevo" : "Usado");

  const isSelled = (num) => (num > 0 ? num + " Vendidos" : "");

  const showCurrency = (currency) => (currency === "ARS" ? "$" : currency);

  const errorOrLoading = (loading, error) => (
    <>
      {loading && <Notification title="Cargando..." />}
      {error && <Notification title="opps! Algo anda mal, revisa la url" />}
    </>
  );

  useEffect(() => {
    if (productStore) {
      setProduct(productStore);
    }
  }, [productStore, setProduct]);

  return (
    <article className="product-detail">
      {product ? (
        <>
          <div className="product-detail__wrapper">
            <div className="product-detail__image">
              <img src={product.picture} alt="logo" />
            </div>
            <div className="product-detail__add_to_cart">
              <div className="product-detail__add_to_cart__wrapper">
                <div className="product-detail__is-new">
                  {isNew(product.new)} - {isSelled(product.sold_quantity)}
                  {product.free_shipping && product.free_methods && (
                    <Tooltip title="Free Shipping">
                      <img src={iconShipping} alt="shipping" />
                    </Tooltip>
                  )}
                </div>
                <div className="product-detail__title">{product.title}</div>
                <div className="product-detail__price">
                  {showCurrency(product.price.currency)}{" "}
                  {formatNumber(product.price.amount)}
                </div>
                <button className="btn-primary product-detail">Comprar</button>
              </div>
            </div>
          </div>
          <div className="product-detail__wrapper">
            <div className="product-detail__description">
              <h3 className="product-detail__description__title">
                Descripción del producto
              </h3>
              <p className="product-detail__description__content">
                {product.description}
              </p>
            </div>
          </div>
        </>
      ) : (
        errorOrLoading(loadingStore, errorStore)
      )}
    </article>
  );
};
