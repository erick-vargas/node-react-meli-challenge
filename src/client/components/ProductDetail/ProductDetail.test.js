import React from "react";
import { shallow, mount } from "enzyme";
import ProductDetail from "./ProductDetail";

describe("ProductDetail component", () => {
  const product = {
    id: '123',
    thumbnail: '456',
    price: 789,
    title: 'my title',
    city: 'my city',
    free_shipping: true,
    free_methods: true,
    description: 'some desc'
  };
  const productDetails = {
    productStore: product,
    loadingStore: false,
    errorStore: false
  };

  it("should match the snapshot", () => {
    const tree = shallow(<ProductDetail productDetails={productDetails} />);
    expect(tree).toMatchSnapshot();
  });

  it("should render 2 Img", () => {
    const wrapper = mount(<ProductDetail productDetails={productDetails} />);
    const find = wrapper.find('img');
    expect(find).toHaveLength(2);
  });

  it("should render price", () => {
    const wrapper = mount(<ProductDetail productDetails={productDetails} />);
    const find = wrapper.find(".product-detail__description__content").text();
    expect(find).toBe("some desc");
  });
});
