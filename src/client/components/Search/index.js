import Search from "./Search";
import { withRouter } from "react-router";
import { connect } from "react-redux";

import { searchProductService } from "../../redux/services/productService";

const mapStateToProps = (state) => ({
  list: JSON.stringify(state)
});

const mapDispatchToProps = (dispatch) => ({
  searchProduct: (value) => searchProductService(value, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Search));
