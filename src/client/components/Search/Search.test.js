import React from "react";
import { shallow, mount } from "enzyme";
import Search from "./Search";

describe("Search component", () => {

  it("should match the snapshot", () => {
    const tree = shallow(<Search />);
    expect(tree).toMatchSnapshot();
  });

  it("should render 2 Img", () => {
    const wrapper = mount(<Search />);
    const find = wrapper.find('img');
    expect(find).toHaveLength(2);
  });
  
});
