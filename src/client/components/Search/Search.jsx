import React, { useState } from "react";
import iconLogo from "../../assets/img/Logo_ML@2x.png";
import iconSearch from "../../assets/img/ic_Search@2x.png";
import "./style.scss";

export default ({ history, searchProduct }) => {
  const handleSearch = (value) => {
    if(value !== '') {
      setValue("");
      setError(null);
      searchProduct(value);
      history.push("/items?search=" + value);
    }else{
      setError("Hey!, Escribe algo para comenzar una busqueda.");
    }
  };

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const [value, setValue] = useState('');
  const [error, setError] = useState(null);

  return (
    <header className="navbar-search">
      <div className="navbar-search__wrapper">
        <div className="navbar-search__image">
          <img src={iconLogo} alt="logo" />
        </div>
        <div className="navbar-search__form__wrapper">
          <div className="navbar-search__form">
            <input
              className={`navbar-search__form__input ${error && 'error'}`}
              type="text"
              placeholder="Nunca dejes de buscar"
              onChange={handleChange}
              value={value}
            />
            <button
              type="submit"
              className="navbar-search__form__btn"
              onClick={() => handleSearch(value)}
            >
              <img
                className="navbar-search__form__btn__icon"
                src={iconSearch}
                alt="icon-search"
              />
            </button>
          </div>
        </div>
      </div>
    </header>
  );
};
