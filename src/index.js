import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Provider } from "react-redux";
import store from "./client/redux/store";

import * as serviceWorker from "./serviceWorker";

import SearchView from "./client/views/SearchView";
import CatalogView from "./client/views/CatalogView";
import ProductView from "./client/views/ProductView";
import NoMatchView from "./client/views/NoMatchView";

import Loading from "./client/components/shared/Loading";

ReactDOM.render(
  <Suspense fallback={<Loading />}>

    <Provider store={store}>

        <Router>
          <Switch>
            <Route path="/" exact>
              <SearchView />
            </Route>
            <Route path="/items" exact>
              <CatalogView />
            </Route>
            <Route path="/items/:id" exact>
              <ProductView catalog />
            </Route>
            <Route>
              <NoMatchView/>
            </Route>

          </Switch>

        </Router>

    </Provider>
    
  </Suspense>,
  document.getElementById("root")
);

serviceWorker.unregister();

