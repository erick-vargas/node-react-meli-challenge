var express = require('express');
var router = express.Router();

var productController = require("../controllers/productController");

/* GET products listing. */
router.get("/items", productController.getProducts);
/* GET product details. */
router.get("/items/:id", productController.getProductDetails);
/* GET categories. */
router.get("/categories/:id", productController.getProductCat);

module.exports = router;
