const fetch = require("node-fetch");

module.exports = {

    serviceRequest: async (url) => {

        let result = await fetch(url)
            .then(result => result.json())
            .then(data => ({
                data, 
                error: null
            }))
            .catch((error) => ({
                data: null, 
                error
            }))
        return result;
    },

}