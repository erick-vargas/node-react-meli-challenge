const { serviceRequest } = require('./service')

const URLS_PRODUCT = {
  SEARCH_PRODUCTS: "https://api.mercadolibre.com/sites/MLA/search?q=",
  PRODUCT_BY_ID: "https://api.mercadolibre.com/items/",
  CAT_BY_ID: "https://api.mercadolibre.com/categories/",
};

module.exports = {
  productSearch: async (query) => {
    const response = await serviceRequest(
      encodeURI(`${URLS_PRODUCT.SEARCH_PRODUCTS}${query.search}`)
    );
    return response;
  },

  findProduct: async (param, full = null) => {
    if (full) param += "/description";
    const response = await serviceRequest(
      encodeURI(URLS_PRODUCT.PRODUCT_BY_ID + param)
    );
    return response;
  },

  findProductCat: async (cat_id) => {
    const response = await serviceRequest(
      encodeURI(`${URLS_PRODUCT.CAT_BY_ID}${cat_id}`)
    );
    return response;
  },
};
